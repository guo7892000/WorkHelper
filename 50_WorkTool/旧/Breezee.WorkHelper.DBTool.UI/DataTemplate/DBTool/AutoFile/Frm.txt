using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Breezee.Core.WinFormUI;
using Breezee.AutoSQLExecutor.Core;
using Breezee.Core.Tool;
using Breezee.Core.IOC;
using Breezee.Core.Entity;
using #NAME_SPACE_I#;

namespace #NAME_SPACE_UI#
{
	public partial class #CLASS_NAME_UI# : BaseForm
	{
		public #CLASS_NAME_UI#()
		{
			InitializeComponent();
		}
	}
}
